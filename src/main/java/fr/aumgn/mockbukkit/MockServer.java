package fr.aumgn.mockbukkit;

import com.avaje.ebean.config.ServerConfig;
import org.apache.commons.lang.NotImplementedException;
import org.bukkit.*;
import org.bukkit.Warning.WarningState;
import org.bukkit.command.CommandException;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.command.PluginCommand;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.help.HelpMap;
import org.bukkit.inventory.*;
import org.bukkit.map.MapView;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.ServicesManager;
import org.bukkit.plugin.messaging.Messenger;
import org.bukkit.scheduler.BukkitScheduler;
import org.bukkit.scoreboard.ScoreboardManager;
import org.bukkit.util.CachedServerIcon;

import java.awt.image.BufferedImage;
import java.io.File;
import java.util.*;
import java.util.logging.Logger;

import static org.mockito.Matchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MockServer implements Server {

    private Map<String, Player> players;
    private Map<String, World> worlds;

    public void init() {
        players = new HashMap<String, Player>();
        worlds = new HashMap<String, World>();
    }

    public Map<String, Player> getPlayersMap() {
        return players;
    }

    public Map<String, World> getWorldsMap() {
        return worlds;
    }

    @Override
    public String getName() {
        return "MockBukkit";
    }

    @Override
    public String getVersion() {
        return "0.0.0";
    }

    @Override
    public String getBukkitVersion() {
        return "0.0-R0";
    }

    @Override
    public Collection<? extends Player> getOnlinePlayers() {
        return players.values();
    }

    @Override
    public Player[] _INVALID_getOnlinePlayers() {
        Collection<Player> onlinePlayers = players.values();
        return onlinePlayers.toArray(new Player[players.size()]);
    }

    @Override
    public int getMaxPlayers() {
        return anyInt();
    }

    @Override
    public int getPort() {
        return anyInt();
    }

    @Override
    public int getViewDistance() {
        return anyInt();
    }

    @Override
    public String getIp() {
        return anyString();
    }

    @Override
    public String getServerName() {
        return anyString();
    }

    @Override
    public String getServerId() {
        return anyString();
    }

    @Override
    public String getWorldType() {
        return anyString();
    }

    @Override
    public boolean getGenerateStructures() {
        return anyBoolean();
    }

    @Override
    public boolean getAllowEnd() {
        return anyBoolean();
    }

    @Override
    public boolean getAllowNether() {
        return anyBoolean();
    }

    @Override
    public boolean hasWhitelist() {
        return anyBoolean();
    }

    @Override
    public void setWhitelist(boolean value) {
    }

    @Override
    public Set<OfflinePlayer> getWhitelistedPlayers() {
        return anySetOf(OfflinePlayer.class);
    }

    @Override
    public void reloadWhitelist() {
    }

    @Override
    public int broadcastMessage(String message) {
        return anyInt();
    }

    @Override
    public String getUpdateFolder() {
        return anyString();
    }

    @Override
    public File getUpdateFolderFile() {
        throw new NotImplementedException("Not implemented in this mock.");
    }

    @Override
    public long getConnectionThrottle() {
        return anyLong();
    }

    @Override
    public int getTicksPerAnimalSpawns() {
        return anyInt();
    }

    @Override
    public int getTicksPerMonsterSpawns() {
        return anyInt();
    }

    @Override
    public Player getPlayer(String rawName) {
        Player found = null;
        String name = rawName.toLowerCase();
        int delta = Integer.MAX_VALUE;
        for (Player player : players.values()) {
            if (player.getName().toLowerCase().startsWith(name)) {
                int curDelta = player.getName().length() - name.length();
                if (curDelta < delta) {
                    found = player;
                    delta = curDelta;
                }
                if (curDelta == 0) break;
            }
        }
        return found;
    }

    @Override
    public Player getPlayerExact(String name) {
        return players.get(name.toLowerCase());
    }

    @Override
    public List<Player> matchPlayer(String rawPattern) {
        List<Player> matchedPlayers = new ArrayList<Player>();
        String pattern = rawPattern.toLowerCase(Locale.ENGLISH);

        for (Player player : players.values()) {
            String playerName = player.getName()
                    .toLowerCase(Locale.ENGLISH);
            if (pattern.equals(playerName)) {
                return Collections.<Player>singletonList(player);
            } else if (playerName.contains(pattern)) {
                matchedPlayers.add(player);
            }
        }

        return matchedPlayers;
    }

    @Override
    public Player getPlayer(UUID uuid) {
        throw new NotImplementedException("Not implemented in this mock.");
    }

    @Override
    public PluginManager getPluginManager() {
        throw new NotImplementedException("Not implemented in this mock.");
    }

    @Override
    public BukkitScheduler getScheduler() {
        throw new NotImplementedException("Not implemented in this mock.");
    }

    @Override
    public ServicesManager getServicesManager() {
        throw new NotImplementedException("Not implemented in this mock.");
    }

    @Override
    public List<World> getWorlds() {
        return new ArrayList<World>(worlds.values());
    }

    public World createWorld(WorldCreator creator) {
        throw new NotImplementedException("Not implemented in this mock.");
    }

    @Override
    public boolean unloadWorld(String name, boolean save) {
        throw new NotImplementedException("Not implemented in this mock.");
    }

    @Override
    public boolean unloadWorld(World world, boolean save) {
        throw new NotImplementedException("Not implemented in this mock.");
    }

    @Override
    public World getWorld(String name) {
        return worlds.get(name);
    }

    public World getWorld(UUID uid) {
        throw new NotImplementedException("Not implemented in this mock.");
    }

    @Override
    public MapView getMap(short id) {
        throw new NotImplementedException("Not implemented in this mock.");
    }

    @Override
    public MapView createMap(World world) {
        throw new NotImplementedException("Not implemented in this mock.");
    }

    @Override
    public void reload() {
        throw new NotImplementedException("Not implemented in this mock.");
    }

    @Override
    public Logger getLogger() {
        return mock(Logger.class);
    }

    @Override
    public PluginCommand getPluginCommand(String name) {
        throw new NotImplementedException("Not implemented in this mock.");
    }

    @Override
    public void savePlayers() {
        throw new NotImplementedException("Not implemented in this mock.");
    }

    public boolean dispatchCommand(CommandSender sender, String commandLine)
            throws CommandException {
        throw new NotImplementedException("Not implemented in this mock.");
    }

    @Override
    public void configureDbConfig(ServerConfig config) {
        throw new NotImplementedException("Not implemented in this mock.");
    }

    @Override
    public boolean addRecipe(Recipe recipe) {
        throw new NotImplementedException("Not implemented in this mock.");
    }

    @Override
    public List<Recipe> getRecipesFor(ItemStack result) {
        throw new NotImplementedException("Not implemented in this mock.");
    }

    @Override
    public Iterator<Recipe> recipeIterator() {
        throw new NotImplementedException("Not implemented in this mock.");
    }

    @Override
    public void clearRecipes() {
        throw new NotImplementedException("Not implemented in this mock.");
    }

    @Override
    public void resetRecipes() {
        throw new NotImplementedException("Not implemented in this mock.");
    }

    @Override
    public Map<String, String[]> getCommandAliases() {
        return anyMapOf(String.class, String[].class);
    }

    @Override
    public int getSpawnRadius() {
        return anyInt();
    }

    @Override
    public void setSpawnRadius(int value) {
    }

    @Override
    public boolean getOnlineMode() {
        return anyBoolean();
    }

    @Override
    public boolean getAllowFlight() {
        return anyBoolean();
    }

    @Override
    public boolean isHardcore() {
        return false;
    }

    @Override
    public boolean useExactLoginLocation() {
        return anyBoolean();
    }

    @Override
    public void shutdown() {
    }

    @Override
    public int broadcast(String message, String permission) {
        throw new NotImplementedException("Not implemented in this mock.");
    }

    @Override
    public OfflinePlayer getOfflinePlayer(String name) {
        String lname = name.toLowerCase(Locale.ENGLISH);
        OfflinePlayer player = getPlayerExact(name);
        if (player == null) {
            player = mock(OfflinePlayer.class);
            when(player.getName()).thenReturn(lname);
        }

        return player;
    }

    @Override
    public OfflinePlayer getOfflinePlayer(UUID uuid) {
        throw new NotImplementedException("Not implemented in this mock.");
    }

    @Override
    public Set<String> getIPBans() {
        throw new NotImplementedException("Not implemented in this mock.");
    }

    @Override
    public void banIP(String address) {
        throw new NotImplementedException("Not implemented in this mock.");
    }

    @Override
    public void unbanIP(String address) {
        throw new NotImplementedException("Not implemented in this mock.");
    }

    @Override
    public Set<OfflinePlayer> getBannedPlayers() {
        throw new NotImplementedException("Not implemented in this mock.");
    }

    @Override
    public BanList getBanList(BanList.Type type) {
        throw new NotImplementedException("Not implemented in this mock.");
    }

    @Override
    public Set<OfflinePlayer> getOperators() {
        throw new NotImplementedException("Not implemented in this mock.");
    }

    @Override
    public GameMode getDefaultGameMode() {
        return any(GameMode.class);
    }

    @Override
    public void setDefaultGameMode(GameMode mode) {
    }

    @Override
    public ConsoleCommandSender getConsoleSender() {
        throw new NotImplementedException("Not implemented in this mock.");
    }

    @Override
    public File getWorldContainer() {
        throw new NotImplementedException("Not implemented in this mock.");
    }

    @Override
    public OfflinePlayer[] getOfflinePlayers() {
        throw new NotImplementedException("Not implemented in this mock.");
    }

    @Override
    public Messenger getMessenger() {
        throw new NotImplementedException("Not implemented in this mock.");
    }

    @Override
    public HelpMap getHelpMap() {
        throw new NotImplementedException("Not implemented in this mock.");
    }

    @Override
    public Inventory createInventory(InventoryHolder owner, InventoryType type) {
        throw new NotImplementedException("Not implemented in this mock.");
    }

    @Override
    public Inventory createInventory(InventoryHolder inventoryHolder, InventoryType inventoryType, String s) {
        throw new NotImplementedException("Not implemented in this mock.");
    }

    @Override
    public Inventory createInventory(InventoryHolder owner, int size) {
        throw new NotImplementedException("Not implemented in this mock.");
    }

    @Override
    public Inventory createInventory(InventoryHolder owner, int size, String title) {
        throw new NotImplementedException("Not implemented in this mock.");
    }

    public int getMonsterSpawnLimit() {
        return anyInt();
    }

    public int getAnimalSpawnLimit() {
        return anyInt();
    }

    public int getWaterAnimalSpawnLimit() {
        return anyInt();
    }

    @Override
    public int getAmbientSpawnLimit() {
        return 0;
    }

    @Override
    public Set<String> getListeningPluginChannels() {
        throw new NotImplementedException("Not implemented in this mock.");
    }

    @Override
    public void sendPluginMessage(Plugin arg0, String arg1, byte[] arg2) {
        throw new NotImplementedException("Not implemented in this mock.");
    }

    @Override
    public String getMotd() {
        throw new NotImplementedException("Not implemented in this mock.");
    }

    @Override
    public String getShutdownMessage() {
        throw new NotImplementedException("Not implemented in this mock.");
    }

    @Override
    public WarningState getWarningState() {
        throw new NotImplementedException("Not implemented in this mock.");
    }

    @Override
    public ItemFactory getItemFactory() {
        throw new NotImplementedException("Not implemented in this mock.");
    }

    @Override
    public ScoreboardManager getScoreboardManager() {
        throw new NotImplementedException("Not implemented in this mock.");
    }

    @Override
    public CachedServerIcon getServerIcon() {
        throw new NotImplementedException("Not implemented in this mock.");
    }

    @Override
    public CachedServerIcon loadServerIcon(File file) throws IllegalArgumentException, Exception {
        throw new NotImplementedException("Not implemented in this mock.");
    }

    @Override
    public CachedServerIcon loadServerIcon(BufferedImage bufferedImage) throws IllegalArgumentException, Exception {
        throw new NotImplementedException("Not implemented in this mock.");
    }

    @Override
    public void setIdleTimeout(int i) {

    }

    @Override
    public int getIdleTimeout() {
        return 0;
    }

    @Override
    public UnsafeValues getUnsafe() {
        throw new NotImplementedException("Not implemented in this mock.");
    }

    @Override
    public boolean isPrimaryThread() {
        return anyBoolean();
    }
}
