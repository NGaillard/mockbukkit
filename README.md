MockBukkit [![Build Status](http://ci.ngaillard.eu/buildStatus/icon?job=MockBukkit)](http://ci.ngaillard.eu/job/MockBukkit)
==========

Mock for Bukkit created by aumgn ([see his repo](https://github.com/aumgn/MockBukkit)). Maintained for the latest versions of Bukkit.

Download:
--------------------

You can found releases and snapshots on [my maven repository](http://sonatype.ngaillard.eu/#view-repositories).
Releases are hand-picked snapshots when they are stable.